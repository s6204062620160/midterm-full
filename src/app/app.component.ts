import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, Form } from '@angular/forms';
import { Flight } from './flight';
import { PageService } from './share/page.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MidFull';
  flight !: Flight[];
  startDate = new Date(Date.now());
  myform !: FormGroup;

  constructor(private form: FormBuilder, public page: PageService){
    this.startDate = new Date(Date.now())
    this.myform = this.form.group({
      name: ['',Validators.required],
      from: [null, Validators.required],
      to: [null, Validators.required],
      triptype: ['', Validators.required],
      departure: ['', Validators.required],
      arrival: ['', Validators.required],
      adults: [0, [Validators.required, Validators.max(10), Validators.pattern('[0-9]*$')]],
      children: [0, [Validators.required, Validators.max(10), Validators.pattern('[0-9]*$')]],
      infants: [0, [Validators.required, Validators.max(10), Validators.pattern('[0-9]*$')]]
    })
  }

  ngOnInit():void{
    this.getPage();
  }

  getPage(){
    this.flight = this.page.getPages();
    console.log("asdasd")
  }
  onSubmit(f:Flight): void{

    const yearD = f.departure.getFullYear() + 543
    const yearA= f.arrival.getFullYear() + 543
    f.departure = new Date((f.departure.getMonth() + 1) + '/' + f.departure.getDate() + "/" + yearD)
    f.arrival = new Date((f.arrival.getMonth() + 1) + '/' + f.arrival.getDate() + "/" + yearA)
    this.flight.push(f);

  }

}
