import { Flight } from "../flight";
export class Mockflight {

  public static Jflight : Flight[] = [
    {
      name: "Thinnaphat",
      from: "Thailand",
      to: "Singapore",
      triptype: "JeT Air",
      departure: new Date('04-02-2565'),
      arrival: new Date('04-06-2565'),
      adults: 1,
      children: 2,
      infants: 3
    }
  ]

}
