import { Injectable } from '@angular/core';
import { Flight } from '../flight';
import { Mockflight } from './mockflight';

@Injectable({
  providedIn: 'root'
})
export class PageService {

  PageFlight: Flight[] = [];

  constructor() {

    this.PageFlight = Mockflight.Jflight

  }
  getPages(){
    return this.PageFlight
  }
  addflight(f:Flight){
    this.PageFlight.push(f)
  }


}
