export class Flight {
  constructor(
  public name: string,
  public from: string,
  public to: string,
  public triptype: string,
  public departure: Date,
  public arrival: Date,
  public adults: number,
  public children: number,
  public infants: number) {}
}
